# AHU-2021-wuzhenlong

## 效果体现
***
![img.png](img.png)

## 项目说明
***
### 架构或依赖说明

- 主框架：springboot  
- 项目管理：gradle
- 数据库交互框架：JPA

### spring mvc 分层结构

#### controller层
实现http接口   

#### service层
服务层，提供主要的逻辑服务

#### dao层
数据库交互层，负责数据库的增删改查等操作

#### mapper层
数据库映射层，将数据库表映射成java对象


### 其他目录

#### exception
封装自定义的异常类

#### factory
设计模式中的工厂方法

### docker说明

#### 启动方式
首先需要安装docker  
powershell中进入Dockerfile所在的目录
1. 使用以下命令构建镜像：  
`docker build . -t ahu-2021-test`  
其中ahu-2021-test为镜像名  
2. 使用以下命令启动镜像:  
`docker run -d -p 8080:8080 ahu-2021-test`