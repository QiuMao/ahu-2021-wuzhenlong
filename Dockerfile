# 从git客户端基础镜像开始构建
# 其中这个镜像是我个人私有的镜像，如果有需要可以联系我分享
FROM registry.cn-hangzhou.aliyuncs.com/qiumao/git-client-jdk11
# 切换工作目录
WORKDIR /home
# 代码仓库ssh地址
ARG GIT_REPOSITORY_SSH=git@gitlab.com:QiuMao/ahu-2021-wuzhenlong.git
# 要部署的分支
ARG GIT_BRANCH=main
# 项目名
ARG PROJECT=ahu-2021-wuzhenlong
# 设置中文
ENV LANG C.UTF-8
# 更新apt
RUN apt update -y \
    # clone代码仓库
    && git clone $GIT_REPOSITORY_SSH
# 进入项目目录里
WORKDIR ./$PROJECT
# 切换到要部署的分支
RUN git checkout $GIT_BRANCH \
    # 给gradlew文件增加可以直接执行的权限
    && chmod +x gradlew \
    # 禁用gradle daemon，节省内存
    && mkdir -p ~/.gradle && echo "org.gradle.daemon=false" >> ~/.gradle/gradle.properties \
    # springboot 打包
    && ./gradlew bootJar
EXPOSE 8080
# 启动springboot项目
CMD java -jar build/libs/ahu-2021-wuzhenlong-0.0.1-SNAPSHOT.jar