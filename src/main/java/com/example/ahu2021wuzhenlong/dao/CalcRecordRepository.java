package com.example.ahu2021wuzhenlong.dao;

import com.example.ahu2021wuzhenlong.mapper.CalcRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author 秋猫
 * @date 2021/10/8 20:57
 * 仓储层
 */
@Repository
public interface CalcRecordRepository extends JpaRepository<CalcRecord, Integer> {
}
