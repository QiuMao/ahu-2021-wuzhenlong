package com.example.ahu2021wuzhenlong.mapper;

import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author 秋猫
 * @date 2021/10/8 20:53
 * calc record表
 */
@Entity
@Table(name = "calc_record")
@Setter
public class CalcRecord {

    /**
     * 数据库主键
     */
    @Id
    private int id;

    /**
     * 运算数1
     */
    private double operand1;

    /**
     * 运算数2
     */
    private double operand2;

    /**
     * 运算符
     */
    private String operator;

    /**
     * 运算结果
     */
    private double result;
}
