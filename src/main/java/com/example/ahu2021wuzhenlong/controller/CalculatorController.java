package com.example.ahu2021wuzhenlong.controller;

import com.example.ahu2021wuzhenlong.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 秋猫
 * @date 2021/10/8 21:21
 * 计算器控制层
 */
@RestController
public class CalculatorController {

    @Autowired
    private CalculatorService calculatorService;

    @GetMapping("/")
    public String hello() {
        return "服务启动成功";
    }

    @GetMapping("/add")
    public String add(@Param("a") double a, @Param("b") double b) {
        try {
            return String.valueOf(calculatorService.dyadicOperation(a, "+", b));
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @GetMapping("/sub")
    public String sub(@Param("a") double a, @Param("b") double b) {
        try {
            return String.valueOf(calculatorService.dyadicOperation(a, "-", b));
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @GetMapping("/multiply")
    public String multiply(@Param("a") double a, @Param("b") double b) {
        try {
            return String.valueOf(calculatorService.dyadicOperation(a, "*", b));
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @GetMapping("/division")
    public String division(@Param("a") double a, @Param("b") double b) {
        try {
            return String.valueOf(calculatorService.dyadicOperation(a, "/", b));
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
