package com.example.ahu2021wuzhenlong;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ahu2021WuzhenlongApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ahu2021WuzhenlongApplication.class, args);
    }

}
