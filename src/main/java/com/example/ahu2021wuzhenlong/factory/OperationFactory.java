package com.example.ahu2021wuzhenlong.factory;

import com.example.ahu2021wuzhenlong.service.DyadicOperation;
import com.example.ahu2021wuzhenlong.service.impl.AddDyadicOperationImpl;
import com.example.ahu2021wuzhenlong.service.impl.DivisionDyadicOperationImpl;
import com.example.ahu2021wuzhenlong.service.impl.MultiDyadicOperationImpl;
import com.example.ahu2021wuzhenlong.service.impl.SubDyadicOperationImpl;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * @author 秋猫
 * @date 2021/10/8 21:08
 * 运算操作工厂方法
 */
@Component
public class OperationFactory {

    /**
     * 根据运算符获取二元运算类
     *
     * @param operator 运算符
     * @return DyadicOperation二元运算类
     */
    public DyadicOperation getOperationByOperator(String operator) {
        HashMap<String, DyadicOperation> map = new HashMap<>(4);
        map.put("+", new AddDyadicOperationImpl());
        map.put("-", new SubDyadicOperationImpl());
        map.put("*", new MultiDyadicOperationImpl());
        map.put("/", new DivisionDyadicOperationImpl());
        return map.get(operator);
    }
}
