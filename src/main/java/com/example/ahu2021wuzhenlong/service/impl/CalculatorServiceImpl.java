package com.example.ahu2021wuzhenlong.service.impl;

import com.example.ahu2021wuzhenlong.dao.CalcRecordRepository;
import com.example.ahu2021wuzhenlong.factory.OperationFactory;
import com.example.ahu2021wuzhenlong.mapper.CalcRecord;
import com.example.ahu2021wuzhenlong.service.CalculatorService;
import com.example.ahu2021wuzhenlong.service.DyadicOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.SystemException;
import java.util.Objects;

/**
 * @author 秋猫
 * @date 2021/10/8 21:13
 * 计算器服务实现类
 */
@Service
public class CalculatorServiceImpl implements CalculatorService {

    @Autowired
    private OperationFactory operationFactory;

    @Autowired
    private CalcRecordRepository calcRecordRepository;

    /**
     * 进行二元运算
     *
     * @param operand1 运算数1
     * @param operator 运算符
     * @param operand2 运算数2
     * @return 运算结果
     */
    @Override
    public double dyadicOperation(double operand1, String operator, double operand2) throws SystemException {

        // 根据运算符获取相应的二元运算类
        DyadicOperation operation = operationFactory.getOperationByOperator(operator);
        if (Objects.isNull(operation)) {
            throw new SystemException(String.format("不支持的运算符: {%s}", operator));
        }

        try {
            // 进行运算
            double result = operation.calculate(operand1, operand2);
            // 保存记录
            CalcRecord calcRecord = new CalcRecord();
            calcRecord.setOperand1(operand1);
            calcRecord.setOperand2(operand2);
            calcRecord.setOperator(operator);
            calcRecord.setResult(result);
            // 提交保存到数据库
            calcRecordRepository.save(calcRecord);
            return result;
        } catch (Exception e) {
            throw new SystemException(String.format("运算时出现错误, msg={%s}", e.getMessage()));
        }
    }
}
