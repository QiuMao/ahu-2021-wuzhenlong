package com.example.ahu2021wuzhenlong.service.impl;

import com.example.ahu2021wuzhenlong.service.DyadicOperation;

/**
 * @author 秋猫
 * @date 2021/10/8 21:05
 * 减法运算
 */
public class SubDyadicOperationImpl implements DyadicOperation {
    /**
     * 进行二元运算
     *
     * @param operand1 运算数1
     * @param operand2 运算数2
     * @return 运算结果
     */
    @Override
    public double calculate(double operand1, double operand2) {
        return operand1 - operand2;
    }
}
