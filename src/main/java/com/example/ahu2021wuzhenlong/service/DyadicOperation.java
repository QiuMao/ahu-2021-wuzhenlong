package com.example.ahu2021wuzhenlong.service;

import org.springframework.stereotype.Service;

/**
 * @author 秋猫
 * @date 2021/10/8 21:02
 * 二元运算服务
 */
public interface DyadicOperation {

    /**
     * 进行二元运算
     *
     * @param operand1 运算数1
     * @param operand2 运算数2
     * @return 运算结果
     */
    double calculate(double operand1, double operand2);
}
