package com.example.ahu2021wuzhenlong.service;

import org.springframework.stereotype.Service;

import javax.transaction.SystemException;

/**
 * @author 秋猫
 * @date 2021/10/8 20:58
 * 计算器服务
 */
@Service
public interface CalculatorService {

    /**
     * 进行二元运算
     *
     * @param operand1 运算数1
     * @param operator 运算符
     * @param operand2 运算数2
     * @return 运算结果
     * @throws SystemException 系统异常
     */
    double dyadicOperation(double operand1, String operator, double operand2) throws SystemException;
}
