package com.example.ahu2021wuzhenlong.exception;

/**
 * @author 秋猫
 * @date 2021/10/8 21:15
 * 业务异常类
 */
public class BusinessException extends Exception{

    public BusinessException(String message) {
        super(message);
    }
}
